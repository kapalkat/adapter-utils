// Set globals
/* global pronghornProps */

// include required items for testing & logging
const assert = require('assert');
const fs = require('fs');
const winston = require('winston');

// stub and attemptTimeout are used throughout the code so set them here
let logLevel = 'none';
const stub = true;
const attemptTimeout = 10000;

// these variables can be changed to run in integrated mode so easier to set
// them here always check these in with bogus data!!!
const host = 'servicenow.dummy.instance';
const username = 'dummy';
const password = 'dummy';

// these are the adapter properties. You generally should not need to alter
// any of these after they are initially set up
global.pronghornProps = {
  pathProps: {
    encrypted: false
  },
  adapterProps: {
    adapters: [{
      id: 'test',
      properties: {
        host,
        port: 443,
        version: 'v1',
        authentication: {
          auth_method: 'basic user_password',
          username,
          password,
          token: 'token',
          token_user_field: 'username',
          token_password_field: 'password',
          token_result_field: 'token',
          token_URI_path: '/authenticate',
          invalid_token_error: 401,
          auth_field: 'header.auth',
          auth_field_format: '{username}:{password}'
        },
        throttle: {
          throttle_enabled: false,
          number_pronghorns: 1,
          sync_async: 'sync',
          max_in_queue: 1000,
          concurrent_max: 1,
          expire_timeout: 0,
          avg_runtime: 200
        },
        request: {
          number_retries: 3,
          limit_retry_error: 0,
          attempt_timeout: attemptTimeout,
          healthcheck_on_timeout: false,
          archiving: false
        },
        proxy: {
          enabled: false,
          host: '',
          port: 1
        },
        ssl: {
          enabled: false,
          accept_invalid_cert: false,
          ca_file: '',
          ciphers: ''
        },
        healthcheck: {
          type: 'startup',
          protocol: 'REST',
          frequency: 60000,
          URI_Path: '/api/now/table/sys_user_group'
        },
        protocol: 'https',
        stub
      },
      type: 'test'
    }]
  }
};

global.$HOME = `${__dirname}/../..`;

// set the log levels that IAP uses, spam and trace are not defaulted
// in so without this you may error on log.trace calls.
const myCustomLevels = {
  levels: {
    spam: 6,
    trace: 5,
    debug: 4,
    info: 3,
    warn: 2,
    error: 1,
    none: 0
  }
};

// need to see if there is a log level passed in
process.argv.forEach((val) => {
  // is there a log level defined to be passed in?
  if (val.indexOf('--LOG') === 0) {
    // get the desired log level
    const inputVal = val.split('=')[1];

    // validate the log level is supported, if so set it
    if (Object.hasOwnProperty.call(myCustomLevels.levels, inputVal)) {
      logLevel = inputVal;
    }
  }
});

// need to set global logging
global.log = winston.createLogger({
  level: logLevel,
  levels: myCustomLevels.levels,
  transports: [
    new winston.transports.Console()
  ]
});

// require the request handler as that is the entry into the Adapter Libraries
const PropUtil = require('../../../lib/propertyUtil');
const TransUtil = require('../../../lib/translatorUtil');

// begin the testing - these should be pretty well defined between the describe
// and the it!
describe('[unit] Adapter Library Test', () => {
  describe('TranslatorUtil Class Tests', () => {
    const propInst = new PropUtil(pronghornProps.adapterProps.adapters[0].id, 'test');
    const a = new TransUtil(pronghornProps.adapterProps.adapters[0].id, propInst);

    // Read the entity schema from the file system
    const schemaFile = 'test/entities/template_entity/schema.json';
    const schemaNoTransFile = 'test/entities/template_entity/noTransSchema.json';
    const schemaDynamciFile = 'test/entities/template_entity/dynamicSchema.json';
    const entitySchema = JSON.parse(fs.readFileSync(schemaFile, 'utf-8'));
    const entityNoTransSchema = JSON.parse(fs.readFileSync(schemaNoTransFile, 'utf-8'));
    const entityDynamicSchema = JSON.parse(fs.readFileSync(schemaDynamciFile, 'utf-8'));
    describe('#mapFromOutboundEntity', () => {
      it('should error - no entity to map', (done) => {
        const p = new Promise((resolve) => {
          try {
            const result = a.mapFromOutboundEntity(null, null);
            // should not have come in here - take exception
            resolve(result);
            done();
          } catch (ex) {
            const error = JSON.parse(ex.message);
            assert.equal('test-translatorUtil-mapFromOutboundEntity', error.origin);
            assert.equal('Missing Data', error.type);
            assert.equal('Entity', error.vars[0]);
            done();
          }
        });
        // log just done to get rid of const lint issue!
        log.debug(p);
      }).timeout(attemptTimeout);
      it('should error - no entity schema', (done) => {
        const p = new Promise((resolve) => {
          const gotObject = { name: 'blahblahblah' };
          try {
            const result = a.mapFromOutboundEntity(gotObject, null);
            // should not have come in here - take exception
            resolve(result);
            done();
          } catch (ex) {
            const error = JSON.parse(ex.message);
            assert.equal('test-translatorUtil-mapFromOutboundEntity', error.origin);
            assert.equal('Missing Data', error.type);
            assert.equal('Entity Schema', error.vars[0]);
            done();
          }
        });
        // log just done to get rid of const lint issue!
        log.debug(p);
      }).timeout(attemptTimeout);
      it('should error - missing required data', (done) => {
        const p = new Promise((resolve) => {
          const gotObject = {
            ph_request_type: 'createEntity', name: 'blahblahblah'
          };
          try {
            const result = a.mapFromOutboundEntity(gotObject, entitySchema);
            // should not have come in here - take exception
            resolve(result);
            done();
          } catch (ex) {
            const error = JSON.parse(ex.message);
            assert.equal('test-translatorUtil-extractJSONEntity', error.origin);
            assert.equal('Schema Validation Failure', error.type);
            assert.equal('should have required property \'.summary\'', error.vars[0]);
            done();
          }
        });
        // log just done to get rid of const lint issue!
        log.debug(p);
      }).timeout(attemptTimeout);
      it('translate a simple object', (done) => {
        const p = new Promise((resolve) => {
          const gotObject = {
            ph_request_type: 'getEntities',
            number: ['CHG000100'],
            short_description: 'blahblahblah'
          };
          const result = a.mapFromOutboundEntity(gotObject, entitySchema);
          resolve(result);
          assert.notEqual(undefined, result);
          assert.notEqual(null, result);
          assert.equal('blahblahblah', result.summary);
          assert.equal('CHG000100', result.number);
          done();
        });
        // log just done to get rid of const lint issue!
        log.debug(p);
      }).timeout(attemptTimeout);
      it('skip translation a simple object', (done) => {
        const p = new Promise((resolve) => {
          const inObject = {
            ph_request_type: 'getEntities', short_description: 'blahblahblah'
          };
          const result = a.mapFromOutboundEntity(inObject, entityNoTransSchema);
          resolve(result);
          assert.notEqual(undefined, result);
          assert.notEqual(null, result);
          assert.equal('blahblahblah', result.short_description);
          done();
        });
        // log just done to get rid of const lint issue!
        log.debug(p);
      }).timeout(attemptTimeout);
      it('translation dynamic fields - simple & inherited', (done) => {
        const p = new Promise((resolve) => {
          const inObject = {
            ph_request_type: 'getEntities',
            short_description: 'blahblahblah',
            applesauce: 'why not have some',
            dynamic: { bananas: 'go crazy' }
          };
          const result = a.mapFromOutboundEntity(inObject, entityDynamicSchema);
          resolve(result);
          assert.notEqual(undefined, result);
          assert.notEqual(null, result);
          assert.equal('blahblahblah', result.summary);
          assert.equal('why not have some', result.applesauce);
          assert.equal('go crazy', result.dynamic.bananas);
          done();
        });
        // log just done to get rid of const lint issue!
        log.debug(p);
      }).timeout(attemptTimeout);
      it('translate an array', (done) => {
        const p = new Promise((resolve) => {
          const gotObject = [
            {
              ph_request_type: 'getEntities',
              short_description: 'blahblahblah'
            },
            {
              ph_request_type: 'getEntities',
              short_description: 'blahblahblah2'
            }
          ];
          const result = a.mapFromOutboundEntity(gotObject, entitySchema);
          resolve(result);
          assert.notEqual(undefined, result);
          assert.notEqual(null, result);
          assert.equal('blahblahblah', result[0].summary);
          done();
        });
        // log just done to get rid of const lint issue!
        log.debug(p);
      }).timeout(attemptTimeout);
      it('translate a comlex object', (done) => {
        const p = new Promise((resolve) => {
          const gotObject = {
            ph_request_type: 'getEntities',
            device: {
              deviceId: 423542, name: 'blahblahblah', location: ['Atlanta']
            }
          };
          const result = a.mapFromOutboundEntity(gotObject, entitySchema);
          resolve(result);
          assert.notEqual(undefined, result);
          assert.notEqual(null, result);
          assert.equal('blahblahblah', result.device.name);
          assert.equal('Atlanta', result.device.location[0]);
          done();
        });
        // log just done to get rid of const lint issue!
        log.debug(p);
      }).timeout(attemptTimeout);
      it('translate a dynamic object', (done) => {
        const p = new Promise((resolve) => {
          const gotObject = {
            ph_request_type: 'getEntities',
            dynamic: {
              donot: 423542, know: 'blahblahblah', fields: ['Atlanta']
            }
          };
          const result = a.mapFromOutboundEntity(gotObject, entitySchema);
          resolve(result);
          assert.notEqual(undefined, result);
          assert.notEqual(null, result);
          assert.equal('blahblahblah', result.dynamic.know);
          done();
        });
        // log just done to get rid of const lint issue!
        log.debug(p);
      }).timeout(attemptTimeout);
    });

    describe('#mapToOutboundEntity', () => {
      it('should error - no entity to map', (done) => {
        const p = new Promise((resolve) => {
          try {
            const result = a.mapToOutboundEntity(null, null);
            // should not have come in here - take exception
            resolve(result);
            done();
          } catch (ex) {
            const error = JSON.parse(ex.message);
            assert.equal('test-translatorUtil-mapToOutboundEntity', error.origin);
            assert.equal('Missing Data', error.type);
            assert.equal('Entity', error.vars[0]);
            done();
          }
        });
        // log just done to get rid of const lint issue!
        log.debug(p);
      }).timeout(attemptTimeout);
      it('should error - no entity schema', (done) => {
        const p = new Promise((resolve) => {
          const inObject = { name: 'blahblahblah' };
          try {
            const result = a.mapToOutboundEntity(inObject, null);
            // should not have come in here - take exception
            resolve(result);
            done();
          } catch (ex) {
            const error = JSON.parse(ex.message);
            assert.equal('test-translatorUtil-mapToOutboundEntity', error.origin);
            assert.equal('Missing Data', error.type);
            assert.equal('Entity Schema', error.vars[0]);
            done();
          }
        });
        // log just done to get rid of const lint issue!
        log.debug(p);
      }).timeout(attemptTimeout);
      it('should error - missing required data', (done) => {
        const p = new Promise((resolve) => {
          const inObject = {
            ph_request_type: 'createEntity', name: 'blahblahblah'
          };
          try {
            const result = a.mapToOutboundEntity(inObject, entitySchema);
            // should not have come in here - take exception
            resolve(result);
            done();
          } catch (ex) {
            const error = JSON.parse(ex.message);
            assert.equal('test-translatorUtil-buildJSONEntity', error.origin);
            assert.equal('Schema Validation Failure', error.type);
            assert.equal('should have required property \'.summary\'', error.vars[0]);
            done();
          }
        });
        // log just done to get rid of const lint issue!
        log.debug(p);
      }).timeout(attemptTimeout);
      it('translate a simple object', (done) => {
        const p = new Promise((resolve) => {
          const inObject = {
            ph_request_type: 'getEntities', summary: 'blahblahblah'
          };
          const result = a.mapToOutboundEntity(inObject, entitySchema);
          resolve(result);
          assert.notEqual(undefined, result);
          assert.notEqual(null, result);
          assert.equal('blahblahblah', result.short_description);
          done();
        });
        // log just done to get rid of const lint issue!
        log.debug(p);
      }).timeout(attemptTimeout);
      it('skip translation a simple object', (done) => {
        const p = new Promise((resolve) => {
          const inObject = {
            ph_request_type: 'getEntities', summary: 'blahblahblah'
          };
          const result = a.mapToOutboundEntity(inObject, entityNoTransSchema);
          resolve(result);
          assert.notEqual(undefined, result);
          assert.notEqual(null, result);
          assert.equal('blahblahblah', result.summary);
          done();
        });
        // log just done to get rid of const lint issue!
        log.debug(p);
      }).timeout(attemptTimeout);
      it('translation dynamic fields - simple & inherited', (done) => {
        const p = new Promise((resolve) => {
          const inObject = {
            ph_request_type: 'getEntities',
            summary: 'blahblahblah',
            applesauce: 'why not have some',
            dynamic: { bananas: 'go crazy' }
          };
          const result = a.mapToOutboundEntity(inObject, entityDynamicSchema);
          resolve(result);
          assert.notEqual(undefined, result);
          assert.notEqual(null, result);
          assert.equal('blahblahblah', result.short_description);
          assert.equal('why not have some', result.applesauce);
          assert.equal('go crazy', result.dynamic.bananas);
          done();
        });
        // log just done to get rid of const lint issue!
        log.debug(p);
      }).timeout(attemptTimeout);
      it('translate an array', (done) => {
        const p = new Promise((resolve) => {
          const inObject = [
            { ph_request_type: 'getEntities', summary: 'blahblahblah' },
            { ph_request_type: 'getEntities', summary: 'blahblahblah2' }
          ];
          const result = a.mapToOutboundEntity(inObject, entitySchema);
          resolve(result);
          assert.notEqual(undefined, result);
          assert.notEqual(null, result);
          assert.equal('blahblahblah', result[0].short_description);
          done();
        });
        // log just done to get rid of const lint issue!
        log.debug(p);
      }).timeout(attemptTimeout);
      it('translate a comlex object', (done) => {
        const p = new Promise((resolve) => {
          const inObject = {
            ph_request_type: 'getEntities',
            device: {
              deviceId: 423542, name: 'blahblahblah', location: ['Atlanta']
            }
          };
          const result = a.mapToOutboundEntity(inObject, entitySchema);
          resolve(result);
          assert.notEqual(undefined, result);
          assert.notEqual(null, result);
          assert.equal('blahblahblah', result.device.name);
          assert.equal('Atlanta', result.device.location[0]);
          done();
        });
        // log just done to get rid of const lint issue!
        log.debug(p);
      }).timeout(attemptTimeout);
      it('translate a dynamic object', (done) => {
        const p = new Promise((resolve) => {
          const gotObject = {
            ph_request_type: 'getEntities',
            dynamic: {
              donot: 423542, know: 'blahblahblah', fields: ['Atlanta']
            }
          };
          const result = a.mapToOutboundEntity(gotObject, entitySchema);
          resolve(result);
          assert.notEqual(undefined, result);
          assert.notEqual(null, result);
          assert.equal('blahblahblah', result.dynamic.know);
          done();
        });
        // log just done to get rid of const lint issue!
        log.debug(p);
      }).timeout(attemptTimeout);
    });

    describe('#formatInputData', () => {
      it('make the input data the right format - object', (done) => {
        const p = new Promise((resolve) => {
          const inObject = { name: 'blahblahblah' };
          const result = a.formatInputData(inObject);
          resolve(result);
          assert.equal('blahblahblah', result.name);
          done();
        });
        // log just done to get rid of const lint issue!
        log.debug(p);
      }).timeout(attemptTimeout);
      it('make the input data the right format - string', (done) => {
        const p = new Promise((resolve) => {
          const inObject = '{ "name": "blahblahblah" }';
          const result = a.formatInputData(inObject);
          resolve(result);
          assert.equal('blahblahblah', result.name);
          done();
        });
        // log just done to get rid of const lint issue!
        log.debug(p);
      }).timeout(attemptTimeout);
    });
  });
});
