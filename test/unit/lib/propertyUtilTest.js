// Set globals
/* global pronghornProps */

// include required items for testing & logging
const assert = require('assert');
const fs = require('fs');
const winston = require('winston');

// stub and attemptTimeout are used throughout the code so set them here
let logLevel = 'none';
const stub = true;
const attemptTimeout = 10000;

// these variables can be changed to run in integrated mode so easier to set
// them here always check these in with bogus data!!!
const host = 'servicenow.dummy.instance';
const username = 'dummy';
const password = 'dummy';

// these are the adapter properties. You generally should not need to alter
// any of these after they are initially set up
global.pronghornProps = {
  pathProps: {
    encrypted: false
  },
  adapterProps: {
    adapters: [{
      id: 'test',
      properties: {
        host,
        port: 443,
        version: 'v1',
        authentication: {
          auth_method: 'basic user_password',
          username,
          password,
          token: 'token',
          token_user_field: 'username',
          token_password_field: 'password',
          token_result_field: 'token',
          token_URI_path: '/authenticate',
          invalid_token_error: 401,
          auth_field: 'header.auth',
          auth_field_format: '{username}:{password}'
        },
        throttle: {
          throttle_enabled: false,
          number_pronghorns: 1,
          sync_async: 'sync',
          max_in_queue: 1000,
          concurrent_max: 1,
          expire_timeout: 0,
          avg_runtime: 200
        },
        request: {
          number_retries: 3,
          limit_retry_error: 0,
          attempt_timeout: attemptTimeout,
          healthcheck_on_timeout: false,
          archiving: false
        },
        proxy: {
          enabled: false,
          host: '',
          port: 1
        },
        ssl: {
          enabled: false,
          accept_invalid_cert: false,
          ca_file: '',
          ciphers: ''
        },
        healthcheck: {
          type: 'startup',
          protocol: 'REST',
          frequency: 60000,
          URI_Path: '/api/now/table/sys_user_group'
        },
        protocol: 'https',
        stub
      },
      type: 'test'
    }]
  }
};

global.$HOME = `${__dirname}/../..`;

// set the log levels that IAP uses, spam and trace are not defaulted
// in so without this you may error on log.trace calls.
const myCustomLevels = {
  levels: {
    spam: 6,
    trace: 5,
    debug: 4,
    info: 3,
    warn: 2,
    error: 1,
    none: 0
  }
};

// need to see if there is a log level passed in
process.argv.forEach((val) => {
  // is there a log level defined to be passed in?
  if (val.indexOf('--LOG') === 0) {
    // get the desired log level
    const inputVal = val.split('=')[1];

    // validate the log level is supported, if so set it
    if (Object.hasOwnProperty.call(myCustomLevels.levels, inputVal)) {
      logLevel = inputVal;
    }
  }
});

// need to set global logging
global.log = winston.createLogger({
  level: logLevel,
  levels: myCustomLevels.levels,
  transports: [
    new winston.transports.Console()
  ]
});

// require the request handler as that is the entry into the Adapter Libraries
const PropUtil = require('../../../lib/propertyUtil');

// begin the testing - these should be pretty well defined between the describe
// and the it!
describe('[unit] Adapter Library Test', () => {
  describe('PropertyUtil Class Tests', () => {
    const a = new PropUtil(pronghornProps.adapterProps.adapters[0].id, 'test/entitiesProp');

    const entityName = 'template_entity';
    const actionName = 'getEntities';
    const badEntityAction1 = 'template_badaction1';
    const badEntityAction2 = 'template_badaction2';
    const badEntityAction3 = 'template_badaction3';
    describe('#getEntitySchema - errors', () => {
      it('error on get entity schema - no entity', (done) => {
        const p = new Promise((resolve) => {
          try {
            a.getEntitySchema(null, null, null, null, (data, error) => {
              if (error) {
                log.error(error);
              }
              // should not have come in here - take exception
              resolve(data);
              done();
            });
          } catch (ex) {
            const error = JSON.parse(ex.message);
            assert.equal('test-propertyUtil-getEntitySchemaFromFS', error.origin);
            assert.equal('Missing Data', error.type);
            assert.equal('Entity', error.vars[0]);
            done();
          }
        });
        // log just done to get rid of const lint issue!
        log.debug(p);
      }).timeout(attemptTimeout);
      it('error on get entity schema - no action', (done) => {
        const p = new Promise((resolve) => {
          try {
            a.getEntitySchema(entityName, null, null, null, (data, error) => {
              if (error) {
                log.error(error);
              }
              // should not have come in here - take exception
              resolve(data);
              done();
            });
          } catch (ex) {
            const error = JSON.parse(ex.message);
            assert.equal('test-propertyUtil-getEntitySchemaFromFS', error.origin);
            assert.equal('Missing Data', error.type);
            assert.equal('Action', error.vars[0]);
            done();
          }
        });
        // log just done to get rid of const lint issue!
        log.debug(p);
      }).timeout(attemptTimeout);
      it('error on get entity schema - bad entity', (done) => {
        const p = new Promise((resolve) => {
          try {
            a.getEntitySchema('garbage', actionName, null, null, (data, error) => {
              if (error) {
                log.error(error);
              }
              // should not have come in here - take exception
              resolve(data);
              done();
            });
          } catch (ex) {
            const error = JSON.parse(ex.message);
            assert.equal('test-propertyUtil-getEntitySchemaFromFS', error.origin);
            assert.equal('Missing File', error.type);
            assert.equal('test/entitiesProp/entities/garbage/action.json', error.vars[0]);
            done();
          }
        });
        // log just done to get rid of const lint issue!
        log.debug(p);
      }).timeout(attemptTimeout);
      it('error on get entity schema - missing action', (done) => {
        const p = new Promise((resolve) => {
          try {
            a.getEntitySchema(entityName, 'garbage', null, null, (data, error) => {
              if (error) {
                log.error(error);
              }
              // should not have come in here - take exception
              resolve(data);
              done();
            });
          } catch (ex) {
            const error = JSON.parse(ex.message);
            assert.equal('test-propertyUtil-getEntitySchemaFromFS', error.origin);
            assert.equal('Invalid Action File', error.type);
            assert.equal('missing action', error.vars[0]);
            assert.equal('test/entitiesProp/entities/template_entity/action.json', error.vars[1]);
            done();
          }
        });
        // log just done to get rid of const lint issue!
        log.debug(p);
      }).timeout(attemptTimeout);
      it('error on get entity schema - missing action file', (done) => {
        const p = new Promise((resolve) => {
          try {
            a.getEntitySchema(badEntityAction3, 'missingActionFile', null, null, (data, error) => {
              if (error) {
                log.error(error);
              }
              // should not have come in here - take exception
              resolve(data);
              done();
            });
          } catch (ex) {
            const error = JSON.parse(ex.message);
            assert.equal('test-propertyUtil-getEntitySchemaFromFS', error.origin);
            assert.equal('Missing File', error.type);
            assert.equal('test/entitiesProp/entities/template_badaction3/action.json', error.vars[0]);
            done();
          }
        });
        // log just done to get rid of const lint issue!
        log.debug(p);
      }).timeout(attemptTimeout);
      it('error on get entity schema - missing actions array', (done) => {
        const p = new Promise((resolve) => {
          try {
            a.getEntitySchema(badEntityAction2, 'missingActionsArray', null, null, (data, error) => {
              if (error) {
                log.error(error);
              }
              // should not have come in here - take exception
              resolve(data);
              done();
            });
          } catch (ex) {
            const error = JSON.parse(ex.message);
            assert.equal('test-propertyUtil-getEntitySchemaFromFS', error.origin);
            assert.equal('Invalid Action File', error.type);
            assert.equal('missing array of actions', error.vars[0]);
            assert.equal('test/entitiesProp/entities/template_badaction2/action.json', error.vars[1]);
            done();
          }
        });
        // log just done to get rid of const lint issue!
        log.debug(p);
      }).timeout(attemptTimeout);
      it('error on get entity schema - action missing protocol', (done) => {
        const p = new Promise((resolve) => {
          try {
            a.getEntitySchema(badEntityAction1, 'missingProtocol', null, null, (data, error) => {
              if (error) {
                log.error(error);
              }
              // should not have come in here - take exception
              resolve(data);
              done();
            });
          } catch (ex) {
            const error = JSON.parse(ex.message);
            assert.equal('test-propertyUtil-getEntitySchemaFromFS', error.origin);
            assert.equal('Invalid Action File', error.type);
            assert.equal('missing protocol', error.vars[0]);
            assert.equal('test/entitiesProp/entities/template_badaction1/action.json', error.vars[1]);
            done();
          }
        });
        // log just done to get rid of const lint issue!
        log.debug(p);
      }).timeout(attemptTimeout);
      it('error on get entity schema - action missing method', (done) => {
        const p = new Promise((resolve) => {
          try {
            a.getEntitySchema(badEntityAction1, 'missingMethod', null, null, (data, error) => {
              if (error) {
                log.error(error);
              }
              // should not have come in here - take exception
              resolve(data);
              done();
            });
          } catch (ex) {
            const error = JSON.parse(ex.message);
            assert.equal('test-propertyUtil-getEntitySchemaFromFS', error.origin);
            assert.equal('Invalid Action File', error.type);
            assert.equal('missing method', error.vars[0]);
            assert.equal('test/entitiesProp/entities/template_badaction1/action.json', error.vars[1]);
            done();
          }
        });
        // log just done to get rid of const lint issue!
        log.debug(p);
      }).timeout(attemptTimeout);
      it('error on get entity schema - action missing path', (done) => {
        const p = new Promise((resolve) => {
          try {
            a.getEntitySchema(badEntityAction1, 'missingPath', null, null, (data, error) => {
              if (error) {
                log.error(error);
              }
              // should not have come in here - take exception
              resolve(data);
              done();
            });
          } catch (ex) {
            const error = JSON.parse(ex.message);
            assert.equal('test-propertyUtil-getEntitySchemaFromFS', error.origin);
            assert.equal('Invalid Action File', error.type);
            assert.equal('missing entity path', error.vars[0]);
            assert.equal('test/entitiesProp/entities/template_badaction1/action.json', error.vars[1]);
            done();
          }
        });
        // log just done to get rid of const lint issue!
        log.debug(p);
      }).timeout(attemptTimeout);
      it('error on get entity schema - action missing schema', (done) => {
        const p = new Promise((resolve) => {
          try {
            a.getEntitySchema(badEntityAction1, 'missingSchema', null, null, (data, error) => {
              if (error) {
                log.error(error);
              }
              // should not have come in here - take exception
              resolve(data);
              done();
            });
          } catch (ex) {
            const error = JSON.parse(ex.message);
            assert.equal('test-propertyUtil-getEntitySchemaFromFS', error.origin);
            assert.equal('Invalid Action File', error.type);
            assert.equal('missing schema', error.vars[0]);
            assert.equal('test/entitiesProp/entities/template_badaction1/action.json', error.vars[1]);
            done();
          }
        });
        // log just done to get rid of const lint issue!
        log.debug(p);
      }).timeout(attemptTimeout);
      it('error on get entity schema - action missing schema file', (done) => {
        const p = new Promise((resolve) => {
          try {
            a.getEntitySchema(badEntityAction1, 'missingSchemaFile', null, null, (data, error) => {
              if (error) {
                log.error(error);
              }
              // should not have come in here - take exception
              resolve(data);
              done();
            });
          } catch (ex) {
            const error = JSON.parse(ex.message);
            assert.equal('test-propertyUtil-getEntitySchemaFromFS', error.origin);
            assert.equal('Missing File', error.type);
            assert.equal('test/entitiesProp/entities/template_badaction1/anotherschema.json', error.vars[0]);
            done();
          }
        });
        // log just done to get rid of const lint issue!
        log.debug(p);
      }).timeout(attemptTimeout);
    });

    // Read the property schema from the file system
    const propertySchema = JSON.parse(fs.readFileSync('schemas/propertiesSchema.json', 'utf-8'));
    let defaultProps = null;
    describe('#setDefaults', () => {
      it('set the default properties from the property schema', (done) => {
        const p = new Promise((resolve) => {
          const result = a.setDefaults(propertySchema);
          resolve(result);
          assert.equal(443, result.port);
          assert.equal(false, result.stub);
          assert.equal('basic user_password', result.authentication.auth_method);
          defaultProps = result;
          done();
        });
        // log just done to get rid of const lint issue!
        log.debug(p);
      }).timeout(attemptTimeout);
    });

    describe('#mergeProperties', () => {
      it('merge the default properties with the provided', (done) => {
        const p = new Promise((resolve) => {
          const props = pronghornProps.adapterProps.adapters[0].properties;
          const result = a.mergeProperties(props, defaultProps);
          resolve(result);
          assert.equal(443, result.port);
          assert.equal(true, result.stub);
          const path = '/api/now/table/sys_user_group';
          assert.equal(path, result.healthcheck.URI_Path);
          done();
        });
        // log just done to get rid of const lint issue!
        log.debug(p);
      }).timeout(attemptTimeout);
    });

    const prop = 'testing';
    let propEncode = '';
    let propEncrypt = '';
    describe('#encryptProperty', () => {
      it('should get null if no property', (done) => {
        const p = new Promise((resolve) => {
          const result = a.encryptProperty(null, null);
          resolve(result);
          assert.equal(null, result);
          done();
        });
        // log just done to get rid of const lint issue!
        log.debug(p);
      }).timeout(attemptTimeout);
      it('should get property back if no technique', (done) => {
        const p = new Promise((resolve) => {
          const result = a.encryptProperty(prop, null);
          resolve(result);
          assert.notEqual(undefined, result);
          assert.equal(prop, result);
          done();
        });
        // log just done to get rid of const lint issue!
        log.debug(p);
      }).timeout(attemptTimeout);
      it('should get base64 encoded property', (done) => {
        const p = new Promise((resolve) => {
          const result = a.encryptProperty(prop, 'base64');
          resolve(result);
          assert.notEqual(undefined, result);
          assert.equal(0, result.indexOf('{code}'));
          propEncode = result;
          done();
        });
        // log just done to get rid of const lint issue!
        log.debug(p);
      }).timeout(attemptTimeout);
      it('should get encrypted property', (done) => {
        const p = new Promise((resolve) => {
          const result = a.encryptProperty(prop, 'encrypt');
          resolve(result);
          assert.notEqual(undefined, result);
          assert.equal(0, result.indexOf('{crypt}'));
          propEncrypt = result;
          done();
        });
        // log just done to get rid of const lint issue!
        log.debug(p);
      }).timeout(attemptTimeout);
    });

    describe('#decryptProperty', () => {
      it('should get null if no property', (done) => {
        const p = new Promise((resolve) => {
          const result = a.decryptProperty(null);
          resolve(result);
          assert.equal(null, result);
          done();
        });
        // log just done to get rid of const lint issue!
        log.debug(p);
      }).timeout(attemptTimeout);
      it('should get property back if can not determine technique', (done) => {
        const p = new Promise((resolve) => {
          const result = a.decryptProperty(prop);
          resolve(result);
          assert.notEqual(undefined, result);
          assert.equal(prop, result);
          done();
        });
        // log just done to get rid of const lint issue!
        log.debug(p);
      }).timeout(attemptTimeout);
      it('should get clear property from base64 encoded property', (done) => {
        const p = new Promise((resolve) => {
          const result = a.decryptProperty(propEncode);
          resolve(result);
          assert.notEqual(undefined, result);
          assert.equal(prop, result);
          done();
        });
        // log just done to get rid of const lint issue!
        log.debug(p);
      }).timeout(attemptTimeout);
      it('should get decrypted property from encrypted property', (done) => {
        const p = new Promise((resolve) => {
          const result = a.decryptProperty(propEncrypt);
          resolve(result);
          assert.notEqual(undefined, result);
          assert.equal(prop, result);
          done();
        });
        // log just done to get rid of const lint issue!
        log.debug(p);
      }).timeout(attemptTimeout);
    });
  });
});
