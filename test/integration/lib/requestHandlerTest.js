// Set globals
/* global pronghornProps */

// include required items for testing & logging
const assert = require('assert');
const fs = require('fs');
const winston = require('winston');

// stub and attemptTimeout are used throughout the code so set them here
let logLevel = 'none';
const stub = true;
const attemptTimeout = 30000;

// these variables can be changed to run in integrated mode so easier to
// set them here always check these in with bogus data!!!
const host = 'servicenow.dummy.instance';
const username = 'dummy';
const password = 'dummy';

// these are the adapter properties. You generally should not need to alter
// any of these after they are initially set up
global.pronghornProps = {
  pathProps: {
    encrypted: false
  },
  adapterProps: {
    adapters: [{
      id: 'template',
      properties: {
        host,
        port: 443,
        authentication: {
          auth_method: 'basic user_password',
          username,
          password,
          token: 'token',
          token_user_field: 'username',
          token_password_field: 'password',
          token_result_field: 'token',
          token_URI_path: '/api/now/table/change_request',
          invalid_token_error: 401,
          auth_field: 'header.auth',
          auth_field_format: '{username}:{password}'
        },
        throttle: {
          throttle_enabled: false,
          number_pronghorns: 1,
          sync_async: 'sync',
          max_in_queue: 1000,
          concurrent_max: 1,
          expire_timeout: 0,
          avg_runtime: 200
        },
        request: {
          number_redirects: 2,
          number_retries: 3,
          limit_retry_error: 0,
          attempt_timeout: attemptTimeout,
          healthcheck_on_timeout: false,
          archiving: false
        },
        proxy: {
          enabled: false,
          host: '',
          port: 1
        },
        ssl: {
          enabled: false,
          accept_invalid_cert: false,
          ca_file: '',
          ciphers: ''
        },
        healthcheck: {
          type: 'startup',
          protocol: 'REST',
          frequency: 60000,
          URI_Path: '/api/now/table/sys_user_group'
        },
        protocol: 'https',
        stub
      },
      type: 'template'
    }]
  }
};

global.$HOME = `${__dirname}/../..`;

// set the log levels that Pronghorn uses, spam and trace are not defaulted
// in so without this you may error on log.trace calls.
const myCustomLevels = {
  levels: {
    spam: 6,
    trace: 5,
    debug: 4,
    info: 3,
    warn: 2,
    error: 1,
    none: 0
  }
};

// need to see if there is a log level passed in
process.argv.forEach((val) => {
  // is there a log level defined to be passed in?
  if (val.indexOf('--LOG') === 0) {
    // get the desired log level
    const inputVal = val.split('=')[1];

    // validate the log level is supported, if so set it
    if (Object.hasOwnProperty.call(myCustomLevels.levels, inputVal)) {
      logLevel = inputVal;
    }
  }
});

// need to set global logging
global.log = winston.createLogger({
  level: logLevel,
  levels: myCustomLevels.levels,
  transports: [
    new winston.transports.Console()
  ]
});

// require the adapter that we are going to be using
const Template = require('../../../lib/requestHandler');

// make sure the good action files are initially in the entities
fs.copyFileSync('test/entities/template_badaction1/actiong.json', 'test/entities/template_badaction1/action.json');
fs.copyFileSync('test/entities/template_badaction2/actiong.json', 'test/entities/template_badaction2/action.json');
fs.copyFileSync('test/entities/template_badaction3/actiong.json', 'test/entities/template_badaction3/action.json');

// begin the testing - these should be pretty well defined between the
// describe and the it!
describe('[integration] Adapter Library Test', () => {
  describe('Request Handler Class Tests', () => {
    const a = new Template(
      pronghornProps.adapterProps.adapters[0].id,
      pronghornProps.adapterProps.adapters[0].properties,
      'test'
    );

    describe('#healthCheck', () => {
      it('should be healthy', (done) => {
        const p = new Promise((resolve) => {
          a.identifyHealthcheck(null, (data) => {
            resolve(data);
            assert.equal('AD.200', data.icode);
            done();
          });
        });
        // log just done to get rid of const lint issue!
        log.debug(p);
      }).timeout(attemptTimeout);
    });

    let entityId = 0;
    const entityName = 'template_entity';
    const addlHeaders = { Accept: 'application/json' };
    const options = { sysparm_limit: 100 };
    describe('#identifyRequest', () => {
      it('identify request - CREATE', (done) => {
        const p = new Promise((resolve) => {
          const dataObj = {
            payload: {
              summary: 'fake summary'
            }
          };
          a.identifyRequest(entityName, 'createEntity', dataObj, true, (data, error) => {
            resolve(data);
            assert.equal(undefined, error);
            assert.notEqual(undefined, data);
            assert.notEqual(null, data);
            assert.notEqual(undefined, data.response);
            assert.notEqual(null, data.response);

            if (stub) {
              assert.equal('CHG0000001', data.response.number);
            } else {
              assert.notEqual(undefined, data.response.number);
              assert.notEqual('', data.response.number);
            }

            entityId = data.response.id;
            done();
          });
        });
        // log just done to get rid of const lint issue!
        log.debug(p);
      }).timeout(attemptTimeout);
      it('identify request - GET', (done) => {
        const p = new Promise((resolve) => {
          const dataObj = {
            addlHeaders
          };
          a.identifyRequest(entityName, 'getEntities', dataObj, true, (data, error) => {
            resolve(data);
            assert.equal(undefined, error);
            assert.notEqual(undefined, data);
            assert.notEqual(null, data);
            assert.notEqual(undefined, data.response);
            assert.notEqual(null, data.response);

            if (stub) {
              assert.equal(1, data.response.length);
              assert.equal('Rollback Oracle Version', data.response[0].summary);
            } else {
              assert.notEqual(0, data.response.length);
            }

            done();
          });
        });
        // log just done to get rid of const lint issue!
        log.debug(p);
      }).timeout(attemptTimeout);
      it('identify request - GET BY ID', (done) => {
        const p = new Promise((resolve) => {
          const dataObj = {
            uriPathVars: [
              entityId
            ]
          };
          a.identifyRequest(entityName, 'getEntities', dataObj, true, (data, error) => {
            resolve(data);
            assert.equal(undefined, error);
            assert.notEqual(undefined, data);
            assert.notEqual(null, data);
            assert.notEqual(undefined, data.response);
            assert.notEqual(null, data.response);

            if (stub) {
              assert.equal('Rollback Oracle Version', data.response.summary);
            } else {
              assert.equal(entityId, data.response.id);
            }

            done();
          });
        });
        // log just done to get rid of const lint issue!
        log.debug(p);
      }).timeout(attemptTimeout);
      it('identify request - GET WITH FILTER', (done) => {
        const p = new Promise((resolve) => {
          const dataObj = {
            uriQuery: {
              number: 'CHG0000001'
            },
            uriOptions: options
          };
          a.identifyRequest(entityName, 'getEntities', dataObj, true, (data, error) => {
            resolve(data);
            assert.equal(undefined, error);
            assert.notEqual(undefined, data);
            assert.notEqual(null, data);
            assert.notEqual(undefined, data.response);
            assert.notEqual(null, data.response);

            if (stub) {
              assert.equal(1, data.response.length);
              assert.equal('Rollback Oracle Version', data.response[0].summary);
            } else {
              assert.notEqual(0, data.response.length);
            }

            done();
          });
        });
        // log just done to get rid of const lint issue!
        log.debug(p);
      }).timeout(attemptTimeout);
      if (stub) {
        it('generic rest request - GET ID WITH FILTER (May break on integration)', (done) => {
          const p = new Promise((resolve) => {
            const dataObj = {
              uriPathVars: [
                entityId
              ],
              uriQuery: {
                number: 'CHG0000001'
              },
              uriOptions: options
            };
            a.identifyRequest(entityName, 'getEntities', dataObj, true, (data, error) => {
              resolve(data);
              assert.equal(undefined, error);
              assert.notEqual(undefined, data);
              assert.notEqual(null, data);
              assert.notEqual(undefined, data.response);
              assert.notEqual(null, data.response);

              if (stub) {
                assert.equal('Rollback Oracle Version', data.response.summary);
              } else {
                assert.equal(entityId, data.response.id);
              }

              done();
            });
          });
          // log just done to get rid of const lint issue!
          log.debug(p);
        }).timeout(attemptTimeout);
      }
      it('identify request - UPDATE', (done) => {
        const p = new Promise((resolve) => {
          const dataObj = {
            payload: {
              id: entityId, description: 'This really is a test case entity'
            },
            uriPathVars: [
              entityId
            ],
            addlHeaders
          };
          a.identifyRequest(entityName, 'updateEntity', dataObj, false, (data, error) => {
            resolve(data);
            assert.equal(undefined, error);
            assert.notEqual(undefined, data);
            assert.notEqual(null, data);
            assert.notEqual(undefined, data.response);
            assert.notEqual(null, data.response);
            assert.equal('success', data.response);
            done();
          });
        });
        // log just done to get rid of const lint issue!
        log.debug(p);
      }).timeout(attemptTimeout);
      if (stub) {
        it('generic rest request - UPDATE WITH FILTER (May break on integration)', (done) => {
          const p = new Promise((resolve) => {
            const dataObj = {
              payload: {
                id: entityId, description: 'This really is a test case entity'
              },
              uriPathVars: [
                entityId
              ],
              uriQuery: {
                id: entityId, number: 'CHG0000001'
              },
              uriOptions: options
            };
            a.identifyRequest(entityName, 'updateEntity', dataObj, false, (data, error) => {
              resolve(data);
              assert.equal(undefined, error);
              assert.notEqual(undefined, data);
              assert.notEqual(null, data);
              assert.notEqual(undefined, data.response);
              assert.notEqual(null, data.response);
              assert.equal('success', data.response);
              done();
            });
          });
          // log just done to get rid of const lint issue!
          log.debug(p);
        }).timeout(attemptTimeout);
      }
      it('identify request - DELETE', (done) => {
        const p = new Promise((resolve) => {
          const dataObj = {
            uriPathVars: [
              entityId
            ]
          };
          a.identifyRequest(entityName, 'deleteEntity', dataObj, false, (data, error) => {
            resolve(data);
            assert.equal(undefined, error);
            assert.notEqual(undefined, data);
            assert.notEqual(null, data);
            assert.notEqual(undefined, data.response);
            assert.notEqual(null, data.response);
            assert.equal('success', data.response);
            done();
          });
        });
        // log just done to get rid of const lint issue!
        log.debug(p);
      }).timeout(attemptTimeout);
      /*
      it('identify request - redirect', (done) => {
        const p = new Promise((resolve) => {
          const dataObj = {
            uriPathVars: [
              301
            ],
            callProperties: {
              host: 'httpstat.us',
              port: 80,
              protocol: 'http',
              stub: false
            }
          };
          a.identifyRequest('template_redirect', 'redirect', dataObj, false, (data, error) => {
            resolve(data);
            assert.equal(undefined, error);
            assert.notEqual(undefined, data);
            assert.notEqual(null, data);
            assert.notEqual(undefined, data.icode);
            assert.notEqual(null, data.icode);
            assert.equal('AD.200', data.icode);
            assert.notEqual(undefined, data.response);
            assert.notEqual(null, data.response);
            done();
          });
        });
        // log just done to get rid of const lint issue!
        log.debug(p);
      }).timeout(attemptTimeout);
      */
    });
  });
});
