// Set globals
/* global pronghornProps noVersionProps */

// include required items for testing & logging
const assert = require('assert');
const fs = require('fs');
const winston = require('winston');

// stub and attemptTimeout are used throughout the code so set them here
let logLevel = 'none';
const stub = true;
const attemptTimeout = 10000;

// these variables can be changed to run in integrated mode so easier to set
// them here always check these in with bogus data!!!
const host = 'servicenow.dummy.instance';
const username = 'dummy';
const password = 'dummy';

// these are the adapter properties. You generally should not need to alter
// any of these after they are initially set up
global.pronghornProps = {
  pathProps: {
    encrypted: false
  },
  adapterProps: {
    adapters: [{
      id: 'template',
      properties: {
        host,
        port: 443,
        version: '',
        authentication: {
          auth_method: 'basic user_password',
          username,
          password,
          token: 'token',
          token_user_field: 'username',
          token_password_field: 'password',
          token_result_field: 'token',
          token_URI_path: '/api/now/table/change_request',
          invalid_token_error: 401,
          auth_field: 'header.auth',
          auth_field_format: '{username}:{password}'
        },
        throttle: {
          throttle_enabled: false,
          number_pronghorns: 1,
          sync_async: 'sync',
          max_in_queue: 1000,
          concurrent_max: 1,
          expire_timeout: 0,
          avg_runtime: 200
        },
        request: {
          number_retries: 3,
          limit_retry_error: 0,
          attempt_timeout: attemptTimeout,
          healthcheck_on_timeout: false,
          archiving: false
        },
        proxy: {
          enabled: false,
          host: '',
          port: 1
        },
        ssl: {
          enabled: false,
          accept_invalid_cert: false,
          ca_file: '',
          ciphers: ''
        },
        healthcheck: {
          type: 'startup',
          protocol: 'REST',
          frequency: 60000,
          URI_Path: '/api/now/table/sys_user_group'
        },
        protocol: 'https',
        stub
      },
      type: 'template'
    }]
  }
};
global.noVersionProps = JSON.parse(JSON.stringify(pronghornProps));

global.$HOME = `${__dirname}/../..`;

// set the log levels that Pronghorn uses, spam and trace are not defaulted
// in so without this you may error on log.trace calls.
const myCustomLevels = {
  levels: {
    spam: 6,
    trace: 5,
    debug: 4,
    info: 3,
    warn: 2,
    error: 1,
    none: 0
  }
};

// need to see if there is a log level passed in
process.argv.forEach((val) => {
  // is there a log level defined to be passed in?
  if (val.indexOf('--LOG') === 0) {
    // get the desired log level
    const inputVal = val.split('=')[1];

    // validate the log level is supported, if so set it
    if (Object.hasOwnProperty.call(myCustomLevels.levels, inputVal)) {
      logLevel = inputVal;
    }
  }
});

// need to set global logging
global.log = winston.createLogger({
  level: logLevel,
  levels: myCustomLevels.levels,
  transports: [
    new winston.transports.Console()
  ]
});

// require the request handler as that is the entry into the Adapter Libraries
const PropUtil = require('../../../lib/propertyUtil');
const Connector = require('../../../lib/connectorRest');
const TransUtil = require('../../../lib/translatorUtil');
const RestHandler = require('../../../lib/restHandler');

// begin the testing - these should be pretty well defined between the describe
// and the it!
describe('[integration] Adapter Library Test', () => {
  describe('Rest Handler Class Tests', () => {
    const propInst = new PropUtil(pronghornProps.adapterProps.adapters[0].id, 'test');
    const transInst = new TransUtil(pronghornProps.adapterProps.adapters[0].id, propInst);
    const connectInst = new Connector(
      pronghornProps.adapterProps.adapters[0].id,
      pronghornProps.adapterProps.adapters[0].properties,
      transInst,
      propInst
    );

    const a = new RestHandler(
      pronghornProps.adapterProps.adapters[0].id,
      pronghornProps.adapterProps.adapters[0].properties,
      connectInst,
      transInst
    );

    describe('#healthCheck', () => {
      it('should be healthy', (done) => {
        const p = new Promise((resolve) => {
          a.healthcheckRest(null, null, (result) => {
            resolve(result);
            assert.notEqual(undefined, result);
            assert.notEqual(null, result);
            assert.notEqual(undefined, result.code);
            assert.notEqual(null, result.code);
            assert.equal('AD.200', result.code);
            assert.notEqual(undefined, result.response);
            assert.notEqual(null, result.response);
            // assert.equal('success', result.response);
            done();
          });
        });
        // log just done to get rid of const lint issue!
        log.debug(p);
      }).timeout(attemptTimeout);
    });

    // Read the internal entity schema from the file system
    const schemaGet = JSON.parse(fs.readFileSync('test/utilsinternalget.json', 'utf-8'));
    const schemaGetFull = JSON.parse(fs.readFileSync('test/utilsinternalgetfull.json', 'utf-8'));
    const schemaPost = JSON.parse(fs.readFileSync('test/utilsinternalpost.json', 'utf-8'));
    const schemaPut = JSON.parse(fs.readFileSync('test/utilsinternalput.json', 'utf-8'));
    const schemaPutQ = JSON.parse(fs.readFileSync('test/utilsinternalputquery.json', 'utf-8'));
    const schemaDelete = JSON.parse(fs.readFileSync('test/utilsinternaldelete.json', 'utf-8'));
    const schemaErrorReq = JSON.parse(fs.readFileSync('test/utilsinternalerrorrequest.json', 'utf-8'));

    let entityId = 0;
    const entityName = 'template_entity';
    const addlHeaders = { Accept: 'application/json' };
    const options = { sysparm_limit: 100 };
    describe('#genericRestRequest', () => {
      it('generic rest request - CREATE', (done) => {
        const p = new Promise((resolve) => {
          const dataObj = {
            payload: {
              summary: 'fake summary'
            }
          };
          a.genericRestRequest(entityName, 'createEntity', schemaPost, dataObj, true, (result) => {
            resolve(result);
            assert.notEqual(undefined, result);
            assert.notEqual(null, result);
            assert.notEqual(undefined, result.icode);
            assert.notEqual(null, result.icode);
            assert.equal('AD.201', result.icode);
            assert.notEqual(undefined, result.response);
            assert.notEqual(null, result.response);
            assert.notEqual(undefined, result.response.id);

            if (stub) {
              assert.equal('CHG0000001', result.response.number);
            } else {
              assert.notEqual('', result.response.number);
            }

            entityId = result.response.id;
            done();
          });
        });
        // log just done to get rid of const lint issue!
        log.debug(p);
      }).timeout(attemptTimeout);
      it('generic rest request - GET', (done) => {
        const p = new Promise((resolve) => {
          const dataObj = {
            addlHeaders
          };
          a.genericRestRequest(entityName, 'getEntities', schemaGet, dataObj, true, (result) => {
            resolve(result);
            assert.notEqual(undefined, result);
            assert.notEqual(null, result);
            assert.notEqual(undefined, result.icode);
            assert.notEqual(null, result.icode);
            assert.equal('AD.200', result.icode);
            assert.notEqual(undefined, result.response);
            assert.notEqual(null, result.response);

            if (stub) {
              assert.equal(1, result.response.length);
              assert.equal('Rollback Oracle Version', result.response[0].summary);
            } else {
              assert.notEqual(0, result.response.length);
            }

            done();
          });
        });
        // log just done to get rid of const lint issue!
        log.debug(p);
      }).timeout(attemptTimeout);
      it('generic rest request - GET BY ID', (done) => {
        const p = new Promise((resolve) => {
          const dataObj = {
            uriPathVars: [
              entityId
            ]
          };
          a.genericRestRequest(entityName, 'getEntities', schemaGet, dataObj, true, (result) => {
            resolve(result);
            assert.notEqual(undefined, result);
            assert.notEqual(null, result);
            assert.notEqual(undefined, result.icode);
            assert.notEqual(null, result.icode);
            assert.equal('AD.200', result.icode);
            assert.notEqual(undefined, result.response);
            assert.notEqual(null, result.response);

            if (stub) {
              assert.equal('Rollback Oracle Version', result.response.summary);
            } else {
              assert.equal(entityId, result.response.id);
            }

            done();
          });
        });
        // log just done to get rid of const lint issue!
        log.debug(p);
      }).timeout(attemptTimeout);
      it('generic rest request - GET WITH FILTER', (done) => {
        const p = new Promise((resolve) => {
          const dataObj = {
            uriQuery: {
              number: 'CHG0000001'
            },
            uriOptions: options
          };
          a.genericRestRequest(entityName, 'getEntities', schemaGet, dataObj, true, (result) => {
            resolve(result);
            assert.notEqual(undefined, result);
            assert.notEqual(null, result);
            assert.notEqual(undefined, result.icode);
            assert.notEqual(null, result.icode);
            assert.equal('AD.200', result.icode);
            assert.notEqual(undefined, result.response);
            assert.notEqual(null, result.response);

            if (stub) {
              assert.equal(1, result.response.length);
              assert.equal('Rollback Oracle Version', result.response[0].summary);
            } else {
              assert.notEqual(0, result.response.length);
            }

            done();
          });
        });
        // log just done to get rid of const lint issue!
        log.debug(p);
      }).timeout(attemptTimeout);
      if (stub) {
        it('generic rest request - GET ID WITH FILTER (May break on integration)', (done) => {
          const p = new Promise((resolve) => {
            const dataObj = {
              uriPathVars: [
                entityId
              ],
              uriQuery: {
                number: 'CHG0000001'
              },
              uriOptions: options
            };
            a.genericRestRequest(entityName, 'getEntities', schemaGet, dataObj, true, (result) => {
              resolve(result);
              assert.notEqual(undefined, result);
              assert.notEqual(null, result);
              assert.notEqual(undefined, result.icode);
              assert.notEqual(null, result.icode);
              assert.equal('AD.200', result.icode);
              assert.notEqual(undefined, result.response);
              assert.notEqual(null, result.response);

              if (stub) {
                assert.equal('Rollback Oracle Version', result.response.summary);
              } else {
                assert.equal(entityId, result.response.id);
              }

              done();
            });
          });
          // log just done to get rid of const lint issue!
          log.debug(p);
        }).timeout(attemptTimeout);
      }
      if (stub) {
        it('generic rest request - GET ID WITH FILTER FULL PATH (May break on integration)', (done) => {
          const p = new Promise((resolve) => {
            const dataObj = {
              uriPathVars: [
                entityId
              ],
              uriQuery: {
                number: 'CHG0000001'
              },
              uriOptions: options
            };
            a.genericRestRequest(entityName, 'getEntitiesFullPath', schemaGetFull, dataObj, true, (result) => {
              resolve(result);
              assert.notEqual(undefined, result);
              assert.notEqual(null, result);
              assert.notEqual(undefined, result.icode);
              assert.notEqual(null, result.icode);
              assert.equal('AD.200', result.icode);
              assert.notEqual(undefined, result.response);
              assert.notEqual(null, result.response);

              if (stub) {
                assert.equal('Rollback Oracle Version', result.response.summary);
              } else {
                assert.equal(entityId, result.response.id);
              }

              done();
            });
          });
          // log just done to get rid of const lint issue!
          log.debug(p);
        }).timeout(attemptTimeout);
        it('generic rest request - GET ID WITH FILTER FULL PATH - No path vars or query (May break on integration)', (done) => {
          const p = new Promise((resolve) => {
            const dataObj = {
              uriOptions: options
            };
            a.genericRestRequest(entityName, 'getEntitiesFullPath', schemaGetFull, dataObj, true, (result) => {
              resolve(result);
              assert.notEqual(undefined, result);
              assert.notEqual(null, result);
              assert.notEqual(undefined, result.icode);
              assert.notEqual(null, result.icode);
              assert.equal('AD.200', result.icode);
              assert.notEqual(undefined, result.response);
              assert.notEqual(null, result.response);

              if (stub) {
                assert.equal('Rollback Oracle Version', result.response[0].summary);
              } else {
                assert.equal(entityId, result.response.id);
              }

              done();
            });
          });
          // log just done to get rid of const lint issue!
          log.debug(p);
        }).timeout(attemptTimeout);
      }
      it('generic rest request - UPDATE', (done) => {
        const p = new Promise((resolve) => {
          const dataObj = {
            payload: {
              id: entityId, description: 'This really is a test case entity'
            },
            uriPathVars: [
              entityId
            ],
            addlHeaders
          };
          a.genericRestRequest(entityName, 'updateEntity', schemaPut, dataObj, false, (result) => {
            resolve(result);
            assert.notEqual(undefined, result);
            assert.notEqual(null, result);
            assert.notEqual(undefined, result.icode);
            assert.notEqual(null, result.icode);
            assert.equal('AD.200', result.icode);
            assert.notEqual(undefined, result.response);
            assert.notEqual(null, result.response);
            assert.equal('success', result.response);
            done();
          });
        });
        // log just done to get rid of const lint issue!
        log.debug(p);
      }).timeout(attemptTimeout);
      if (stub) {
        it('generic rest request - UPDATE WITH FILTER (May break on integration)', (done) => {
          const p = new Promise((resolve) => {
            const dataObj = {
              payload: {
                id: entityId, description: 'This really is a test case entity'
              },
              uriPathVars: [
                entityId
              ],
              uriQuery: {
                id: entityId, number: 'CHG0000001'
              },
              uriOptions: options
            };
            a.genericRestRequest(entityName, 'updateEntity', schemaPutQ, dataObj, false, (result) => {
              resolve(result);
              assert.notEqual(undefined, result);
              assert.notEqual(null, result);
              assert.notEqual(undefined, result.icode);
              assert.notEqual(null, result.icode);
              assert.equal('AD.200', result.icode);
              assert.notEqual(undefined, result.response);
              assert.notEqual(null, result.response);
              assert.equal('success', result.response);
              done();
            });
          });
          // log just done to get rid of const lint issue!
          log.debug(p);
        }).timeout(attemptTimeout);
      }
      it('generic rest request - DELETE', (done) => {
        const p = new Promise((resolve) => {
          const dataObj = {
            uriPathVars: [
              entityId
            ]
          };
          a.genericRestRequest(entityName, 'deleteEntity', schemaDelete, dataObj, false, (result) => {
            resolve(result);
            assert.notEqual(undefined, result);
            assert.notEqual(null, result);
            assert.notEqual(undefined, result.icode);
            assert.notEqual(null, result.icode);
            assert.equal('AD.200', result.icode);
            assert.notEqual(undefined, result.response);
            assert.notEqual(null, result.response);
            assert.equal('success', result.response);
            done();
          });
        });
        // log just done to get rid of const lint issue!
        log.debug(p);
      }).timeout(attemptTimeout);
      it('generic rest request - GET BUT RETURN 404', (done) => {
        const p = new Promise((resolve) => {
          const dataObj = {
            addlHeaders
          };
          a.genericRestRequest(entityName, 'errorrequest', schemaErrorReq, dataObj, true, (result, error) => {
            // should not have come in here - take exception
            resolve(result);
            assert.equal('template-connectorRest-handleEndResponse', error.IAPerror.origin);
            assert.equal('AD.500', error.icode);
            assert.equal('Error 404 received on request', error.IAPerror.displayString);
            done();
          });
        });
        // log just done to get rid of const lint issue!
        log.debug(p);
      }).timeout(attemptTimeout);
    });
  });

  describe('Rest Handler No Version Properties Class Tests', () => {
    delete noVersionProps.adapterProps.adapters[0].properties.version;

    const propInst = new PropUtil(noVersionProps.adapterProps.adapters[0].id, 'test');
    const transInst = new TransUtil(noVersionProps.adapterProps.adapters[0].id, propInst);
    const connectInstB = new Connector(
      noVersionProps.adapterProps.adapters[0].id,
      noVersionProps.adapterProps.adapters[0].properties,
      transInst,
      propInst
    );
    const b = new RestHandler(
      noVersionProps.adapterProps.adapters[0].id,
      noVersionProps.adapterProps.adapters[0].properties,
      connectInstB,
      transInst
    );

    // Read the internal entity schema from the file system
    const schemaGetFull = JSON.parse(fs.readFileSync('test/utilsinternalgetfull.json', 'utf-8'));
    const schemaDelete = JSON.parse(fs.readFileSync('test/utilsinternaldelete.json', 'utf-8'));

    const entityId = 0;
    const entityName = 'template_entity';
    const options = { sysparm_limit: 100 };
    describe('#genericRestRequest', () => {
      if (stub) {
        it('generic rest request - GET ID WITH FILTER FULL PATH (May break on integration)', (done) => {
          const p = new Promise((resolve) => {
            const dataObj = {
              uriPathVars: [
                entityId
              ],
              uriQuery: {
                number: 'CHG0000001'
              },
              uriOptions: options
            };
            b.genericRestRequest(entityName, 'getEntitiesFullPath', schemaGetFull, dataObj, true, (result) => {
              resolve(result);
              assert.notEqual(undefined, result);
              assert.notEqual(null, result);
              assert.notEqual(undefined, result.icode);
              assert.notEqual(null, result.icode);
              assert.equal('AD.200', result.icode);
              assert.notEqual(undefined, result.response);
              assert.notEqual(null, result.response);

              if (stub) {
                assert.equal('Rollback Oracle Version', result.response.summary);
              } else {
                assert.equal(entityId, result.response.id);
              }

              done();
            });
          });
          // log just done to get rid of const lint issue!
          log.debug(p);
        }).timeout(attemptTimeout);
      }
      it('generic rest request - DELETE', (done) => {
        const p = new Promise((resolve) => {
          const dataObj = {
            uriPathVars: [
              entityId
            ]
          };
          b.genericRestRequest(entityName, 'deleteEntity', schemaDelete, dataObj, false, (result) => {
            resolve(result);
            assert.notEqual(undefined, result);
            assert.notEqual(null, result);
            assert.notEqual(undefined, result.icode);
            assert.notEqual(null, result.icode);
            assert.equal('AD.200', result.icode);
            assert.notEqual(undefined, result.response);
            assert.notEqual(null, result.response);
            assert.equal('success', result.response);
            done();
          });
        });
        // log just done to get rid of const lint issue!
        log.debug(p);
      }).timeout(attemptTimeout);
    });
  });
});
